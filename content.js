// Toast {{{
////
//// Show Toast Style Message From JavaScript.
///////////////////////////////////////////////////////////////////////////////////////
var timer;
var toastElement;

///
/// Method For Show Toast Style Message
/// @param message  Meesage String
/// @param second   Second For Message Showing. Or Default Setting Time(=3second)
///
function showToast(message, rect_top, rect_right, second) {
    var divElement = document.createElement("div");
    var spanElement = document.createElement("span");
    setToastStyle(divElement, spanElement);
    spanElement.innerText = message;
    divElement.appendChild(spanElement);

    document.body.appendChild(divElement);

    // Adjust Message Show Place
    divElement.style.position = "absolute";
    divElement.style.left = (rect_right + 20) + "px";
    divElement.style.top = rect_top + "px";
    toastElement = divElement;

    var from = 0;
    var distance = 0.8;
    var duration = 1000;
    var begin = new Date() - 0;
    var id = setInterval(function() {
            var time = new Date() - begin;
            var current = easing(time, from, distance, duration);
            if( time > duration ) {
                clearInterval(id);
                current = from + distance;

                // Remove Toast From Display
                timer = setTimeout("removeToast()", second * 1000);
            } else {
                toastElement.style.opacity = current;
                toastElement.style.transparent = current;
            }
    }, 10);
}

///
/// Style Setting For Toast Style Message.
///
function setToastStyle(divElement, spanElement) {
    divElement.style.padding = "10px 70px";
    spanElement.style.padding = "3px";
    divElement.style.backgroundColor = "#b8d200";
    divElement.style.borderRadius = "1em";
    divElement.style.border = "1px solid red";

    divElement.style.opacity = 0;
    divElement.style.transparent = 0;

}

///
/// Remove Toast From Display
///
function removeToast() {
    if( toastElement ) {
        var from = 0.8;
        var distance = -0.8;
        var duration = 1000;
        var begin = new Date() - 0;
        var id = setInterval(function() {
                var time = new Date() - begin;
                var current = easing(time, from, distance, duration);
                if( time > duration ) {
                    clearInterval(id);
                    current = from + distance;
                    document.body.removeChild(toastElement);
                    toastElement = null;
                } else {
                    toastElement.style.opacity = current;
                    toastElement.style.transparent = current;
                }
        }, 100);
    }
}

function easing( time, from, distance, duration ) {
    return distance * time / duration + from;
}
// }}}

function getTask() {
    let arr = location.pathname.split("/");
    let i = arr.indexOf("tasks");
    if (i == -1) {
        return undefined;
    }
    return arr[i+1];
}

var contestId = undefined;
var problemIndex = undefined;   // a,b,c,d,e,...
async function updateProblemInformation() {
    const task = getTask();
    if (task == undefined) {
        let url = location.pathname.split('/');
        let i = url.indexOf('contests');
        contestId = url[i + 1];
    } else {
        const res = await fetch("https://kenkoooo.com/atcoder/resources/problems.json");
        const problems = await res.json();
        let problemInfo = problems.filter((prob) => prob['id'] == task)[0];
        contestId = problemInfo['contest_id'];
        problemIndex = problemInfo['problem_index'].toLowerCase();
    }
}
updateProblemInformation();

function getText() {
    const task = getTask();
    if (task == undefined) {
        return contestId;
    } else {
        return contestId + '/' + problemIndex;
    }
}

let elem = document.createElement('a');
if (location.pathname.indexOf('/tasks') != -1) {
    elem.className = 'btn btn-default btn-sm';
} else {
    elem.className = 'btn btn-default mt-2';
}
elem.textContent = ' ContestID';
elem.addEventListener('click', function(){
    let url = location.pathname.split('/');
    let i = url.indexOf('contests');
    let contestId = url[i + 1];
    let rect = elem.getBoundingClientRect();
    let message = getText();
    navigator.clipboard.writeText(message).then(
        () => {
            showToast("コピーが成功しました!!\n" + getText(message), rect.top, rect.right, 2);
        },
        () => {
            showToast("コピーが失敗しました!!", rect.top, rect, 2);
        }
    );
})

let figElem = document.createElement('span');
figElem.className = 'glyphicon glyphicon-file';
figElem.setAttribute('aria-hidden', 'true');
elem.prepend(figElem);

if (location.pathname.indexOf('/tasks') != -1) {
    let parentElem = document.getElementsByClassName("h2")[0];
    parentElem.appendChild(elem);
} else {
    let parentElem = document.getElementsByClassName("insert-participant-box")[0];
    let pElem = document.createElement('p');
    pElem.className = 'text-center';
    pElem.appendChild(elem);
    parentElem.appendChild(pElem);
}
